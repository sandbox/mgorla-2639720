CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This module creates a new data alter callback that allows to filter nodes
associated to target taxonomy terms during Solr indexing process.
This filter is very useful when you have, for example, a single content type
but you need to create multiple indexes that filter a subset of the nodes of
that content type.


REQUIREMENTS
------------
This module adds a filter for SearchAPI Solr Indexs configured in your drupal
site. Therefore it requires the following modules:

* Search API (https://www.drupal.org/project/search_api)


INSTALLATION
------------
To install the module, use the canonical method:
* download the module and uncompress it
  in the contrib modules folder of your website
* enable the module by going to admin/modules path


CONFIGURATION
-------------
Once the module is enabled you should be able to activate the new filter
in the <strong>Filter</strong> tab inside any SearchAPI index you've already
configured in your Drupal site. From this page you should be able to configure
the <strong>Vocabularies</strong> whose terms you want to associate
to the current index.

Now that the filter is configured, it won't affect the indexing yet:
you'll have to enable the single terms of that vocabulary that you want
to be used as filter. Editing each of the enabled vocabularies term, t
he user will be shown a new "Search API Taxonomy Term" fieldset
that will allow her to associate the single term with
the SearchAPI indexes that have enabled that vocabulary.
