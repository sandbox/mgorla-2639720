<?php

/**
 * @file
 * callback.inc
 */

/**
 * Search API data alteration callback.
 */
class SearchApiTaxonomyTermFilter extends SearchApiAbstractAlterCallback {
  /**
   * Implements SearchApiAlterCallbackInterface::__construct().
   */
  public function __construct(SearchApiIndex $index, array $options = array()) {
    $this->index = $index;
    $this->options = $options;
    $this->term_list = search_api_taxonomy_term_get_terms_of_index($this->index->machine_name);
    $this->node_list = search_api_taxonomy_term_get_nodes_of_index($this->index->machine_name);
  }

  /**
   * Alter items.
   */
  public function alterItems(array &$items) {
    // Match between current enabled tid/node and nids of the items.
    foreach ($items as $key => $item) {
      if (!in_array($item->nid, $this->node_list)) {
        unset($items[$key]);
      }
    }
  }

  /**
   * Configuration form.
   *
   * The form will expose the current site vocabularies as checkboxes
   * and a list of the current taxonomy terms that are associated to the current
   * filter.
   */
  public function configurationForm() {
    // Fieldset container to show the terms associated
    // to the current filter.
    $form['search_api_taxonomy_term'] = array(
      '#title'        => 'Search API Taxonomy Terms',
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#weight'       => -1,
      '#description'  => t('The list of taxonomy terms currently enabled to filter this index.'),
    );
    $form['search_api_taxonomy_term']['search_api_taxonomy_term_vocabularies'] = array(
      '#markup'  => (!empty($this->term_list)) ? $this->getRenderedTermList() : t('No terms were associated to this SolrIndex.'),
    );
    // Checkbox field to list the site vocabularies.
    $form[SEARCH_API_TAXONOMY_TERM_FILTER_FIELD] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled vocabularies'),
      '#default_value' => isset($this->options[SEARCH_API_TAXONOMY_TERM_FILTER_FIELD]) ? $this->options[SEARCH_API_TAXONOMY_TERM_FILTER_FIELD] : array(),
      '#options' => $this->getVocabulariesList(),
      '#description' => t('Terms of the selected vocabularies will be available to be selected as term filters.'),
    );
    return $form;
  }

  /**
   * Helper to return a list of vocabularies as options for a checkbox field.
   *
   * @return array
   *   An array of checkboxes options.
   */
  private function getVocabulariesList() {
    $vocabularies = taxonomy_get_vocabularies();
    $options = array();
    foreach ($vocabularies as $voc) {
      $vocabulary_link = l(t('Edit terms'), 'admin/structure/taxonomy/' . $voc->machine_name . '/indexes');
      $options[$voc->machine_name] = t('@vocabulary_name [ !vocabulary_link ]', array(
        '@vocabulary_name' => $voc->name,
        '!vocabulary_link' => $vocabulary_link,

      ));
    }
    return $options;
  }

  /**
   * Returns a rendered list of taxonomy terms for this index.
   *
   * @return mixed
   *   Themed html list of terms.
   */
  private function getRenderedTermList() {
    $term_list = $this->term_list;
    $list_markup = NULL;
    $vocabularies = taxonomy_get_vocabularies();
    if (!empty($term_list)) {
      $items_list = array();
      foreach ($term_list as $tid) {
        $term                   = taxonomy_term_load($tid);
        $term_name              = l($term->name, 'taxonomy/term/' . $tid . '/edit');
        $term_vocabulary_name   = $vocabularies[$term->vid]->name;
        $list_element = t('!term_name (@term_vocabulary)', array(
          '!term_name'        => $term_name,
          '@term_vocabulary'  => $term_vocabulary_name,
        ));
        $items_list[] = $list_element;
      }
      $list_markup = theme('item_list', array('items' => $items_list));
    }
    return $list_markup;
  }

}
